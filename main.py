# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
# if __name__ == '__main__':
#    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/

####################################
####################################
## 100 Page Python Intro
## Source: https://learnbyexample.github.io/100_page_python_intro/
####################################
####################################


####################################
## 4. Strings and user input
####################################


print('*************')
print('Hello there!')
print('*************')

# 'qwerty'.encode('utf-16')
#
# greeting = 'Hy there\nHow are you?\t and you achille?'
# print(greeting)
#
# word = 'buffalo '
# print(word * 8)
# greeting = 'voila' ' ' 'un ' 'groupe'
# print(greeting)
#
# firstname = 'John'
# lastname = 'Grant'
# greeting = f'Bonjour {firstname} {lastname}'
# print(greeting)
# realmeetings = 3
# virtualmeetings = 9
# greeting = f'Bonjour {firstname} {lastname} for the {realmeetings + virtualmeetings =}!'
# print(greeting)

num = 42
f'{num:b}'
f'{num:x}'
f'{num:#x}'

title = 'Architecture'
f'{title:^20}'
f'{title:=^20}'
f'{title:=<20}'
# 'Architecture========'
f'{title:=>20}'
# '========Architecture'

num1 = 78
num2 = 45
'output = {} + {} = {}'.format(num1, num2, num1 + num2)

# name = input('Name:')
# type(name)

# price = input('Price:')
# type(price)
# float(price)
# type(float(price))

'qwerty'.encode('utf-16')


####################################
## 5. Defining functions
####################################

def greeting():
    print('-----------------------------')
    print('         Hello World         ')
    print('-----------------------------')


# greeting()


def greeting2(ip):
    op_length = 10 + len(ip)
    styled_line = '-' * op_length
    print(styled_line)
    print(f'{ip:^{op_length}}')
    print(styled_line)


# greeting2('hello world')
# weather = 'Today would be a nice, sunny day'
# greeting2(weather)


def greeting3(ip, style='-', spacing=10):
    op_length = spacing + len(ip)
    styled_line = style * op_length
    print(styled_line)
    print(f'{ip:^{op_length}}')
    print(styled_line)
    print('ip={} ; style={} ; spacing= ; op_length={}'.format(ip, style, spacing, op_length))


# greeting3('hi')
# greeting3('bye', spacing=5)
# greeting3('hello', style='=')
# greeting3('good day', ':', 2)


#
# greeting3(spacing=5, ip='Oh!')
#
# value = print('hi')
# print(value)
# type(value)


def num_square(n):
    """
    Returns the square of a number
    :param n: number
    :return: number*number
    """
    return n * n


# value = num_square(4)
#
# print(value)
# print('hi', 'peter', 9, sep='', end='\n=====\n')

# DOCSTRINGS

# help(num_square) # press q to exit.


####################################
## 6. Control structures
####################################


0 == 4
0 == 2 - 2
'aaa' < 'bbb'
'ccc' < 'bbb'
bool(5)
bool(0)
bool('abc')
bool('')
bool(None)
2 < 4 and 8 > 4
2 < 4 and 8 > 14
2 < 4 or 8 > 14
not (2 < 4 or 8 > 14)
2 < 3 < 4
num = 5
num > 3 and num <= 5
3 < num <= 5

num in range(3, 6)
num == 0 or num == 5 or num == 10
num in (0, 5, 10)

'cat' not in ('bat', 'mat', 'pat')

# When applied to strings, the in operator performs substring comparison.
'va' in 'cheval'


def isodd(n):
    if n % 2:
        return True
    else:
        return False


def iseven(n):
    return not n % 2


# print('isodd:')
# print(f'{isodd(42) = }')
# print(f'{isodd(-21) = }')
# print(f'{isodd(123) = }')
# print('iseven')
# print(f'{iseven(3) = }')
# print(f'{iseven(8) = }')


def abs(num):
    return num if num >= 0 else -num


# print(f'{abs(4) = }')
# print(abs(-0.0))

def my_loop():
    my_range = range(3)
    my_range = range(3, 27, 3)
    my_range = range(-2, 14, 3)
    print(list(my_range))
    # for i in my_range:
    #     print(f'{i = }')


# my_loop()

# num = int(input('Enter an int: '))
# while num > 0:
#     num -= 1
#     print(num)
# print('THE END')


# while True:
#     num = input('Enter an int: ')
#     if not num:
#         break
#     print(f'square root of {num} is {float(num)**0.5:.4f}')

# for num in range(0, 10, 3):
#     # if num % 3:
#     #     continue
#     print(f'{num} * 2 = {num * 2}')
#
# for n in range(2, 20):
#     for m in range(2, n):
#         if n % m == 0:
#             break
#     else:
#         print(f'{n} is a prime number.')


def work_in_progress():
    pass


# # Fizzbuzz game:
# for i in range(100):
#     output = ''
#     if i % 3 == 0:
#         output += 'Fizz'
#     if i % 5 == 0:
#         output += 'Buzz'
#     # if output == '':
#     #     print(i)
#     # else:
#     #     print(output)
#     print(output if output != '' else i)


# Exercise: Print all numbers from 1 to 1000 (inclusive) which reads
# the same in reversed form in both binary and decimal format. For
# example, 33 in decimal is 100001 in binary and both of these are
# palindromic.

# limit = 1000
# print(f'List of all integers from 0 to {limit} that read the same in reverse form in decimal and binary:')
# for i in range(limit):
#     i_bin_string = f'{i:b}'
#     i_dec_string = f'{i:d}'
#     if i_dec_string == i_dec_string[::-1] and i_bin_string == i_bin_string[::-1]:
#         print(i_dec_string, i_bin_string, sep=':')


# # returns the maximum nested depth of curly braces for a given string input.
# formula = '{{a+2}*{{b+{c*d}}+e*d}}'
# formula = '{a}*b{'
# formula = '}a+b{'
# depth_error = False
# depth = 0
# max_depth = 0
# for c in formula:
#     if c == '{':
#         depth += 1
#     elif c == '}':
#         # if too much closing: flag error and early exit
#         if depth == 0:
#             depth_error = True
#             break
#         # record this peak depth if greater than max depth so far and decrease current depth
#         max_depth = max(max_depth, depth)
#         depth -= 1
# # check for good closing:
# depth_error = depth_error or depth > 0
# if depth_error:
#   print(f'Bracket ERROR in "{formula}"')
# else:
#     print(f'"{formula}" max depth={max_depth}')


