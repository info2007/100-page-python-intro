### MUTABILITY ###

# int, float, str and tuple are examples for immutable data types. On
# the other hand, types like list and dict are mutable.

## id
num1 = 5
print(id(num1))  # the id/reference of the variable
num1 = 10
print(id(num1))  # different ID because new value
num2 = num1
print(id(num1), id(num2))  # same reference is shared
num2 = 11
print(id(num1), id(num2))  # num2 have new reference, num1 unchanged

num_2d = ([1, 2, 3, 4], [2, 4, 8, 16], [2, 3, 5, 7])
last_2 = num_2d[-2:]
last_2[0][-1] = 'sixteen'
last_2[1][-1] = 'seven'
print(num_2d) # change reflected in the original var because list is mutable
print(f'{id(num_2d) = } ; {id(last_2) = }')
print(f'{id(num_2d[-2]) = } ; {id(last_2[0]) = }')

## Slicing notation shallow copy

test = [1, 'one', 1.0]  # all immutables
copy_test = test[:]
print(id(test) == id(copy_test))  # different
print(id(test[0]) == id(copy_test[0]))  # equal
copy_test[0] += 1000
print(id(test[0]) == id(copy_test[0]))  # now diff: updating immutable has created a new variable

test = [[1, 2, 3, 4], ['one', 'two', 'three', 'four']]
copy_test = test[:]
copy_test[0][-1] += 4440
print(test)  # change the original!

## copy.deepcopy
import copy
test = [[1, 2, 3, 4], ['one', 'two', 'three', 'four'], [1.0, 2.0, 3.0, 4.0]]
copy_test = copy.deepcopy(test)
copy_test[0][-1] += 4440
print(test)  # did NOT change the original!

# exercise: 2 first element are deepcopy, third one is not.
mixed_copy = []
mixed_copy.extend(copy.deepcopy(test[:2]))
mixed_copy.extend(test[2:])
mixed_copy[0][0] += 1110
mixed_copy[2][0] += 1110
print(f'{test = } {mixed_copy = }')
