import string
import re

# join
print(1, 2)
# ' '.join(1, 2)  # does not work, cause need to be 1 iterable? of str
print(' - '.join(('1', '2', '3')))

# Transliteration
print(ord('a'), ord('b'), ord('c'))
print(ord('A'), ord('B'), ord('C'))
buffer = ''
for i in range(65, 65 + 26):
    buffer += chr(i)
print(buffer)

buffer = ''
for i in range(32, 126):
    buffer += chr(i)
print(buffer)

mapping = str.maketrans('aeiouy', 'AEIOUY')
print(mapping)
greeting = 'Bonjour, bonne journee!'
print(greeting.translate(mapping))

print(string.punctuation)
print(string.ascii_uppercase)
print(string.hexdigits)

greeting = 'This is ""a"" [{sentence}] with a "lot" of *extra* punctuations. (Name LastName)'
print(greeting.translate(str.maketrans('', '', string.punctuation)))
chars_to_delete = ''.join(set(string.punctuation) - set('().'))
print(chars_to_delete)
print(greeting.translate(str.maketrans('', '', chars_to_delete)))

# Removing leading/trailing characters
greeting = ' \t   voila   \n';
print(greeting.strip())
chaine = 'preflightpost'
print(chaine.removeprefix('pre').removesuffix('post'))

# Dealing with case
sentence = 'thIs iS a saMple StrIng'
print(sentence.lower())
print(sentence.capitalize())
print(sentence.title())
sentence = 'a_long_variable_short-name';
print(sentence.title())
print(string.capwords(sentence, '_'))

phrase = 'The Sun Is Shinning.'
# phrase = 'blabla'
print(phrase.islower())
print(phrase.isalpha())
print(phrase.istitle())

phrase = 'This is a simple string'
print(phrase)
print('is' in phrase)
print('are' in phrase)
print(phrase.count('is'))
print(phrase.endswith('ing'))
print(phrase.endswith(('ing', 'ed')))

line = 'Peter;Brien;;34;plumber;900'
print(line.split(';', maxsplit=3))

lines = '''Peter;Brien;;34;plumber;900
Jack;Smith;1;45;electrician;860'''
print(lines)

phrase = '2 be or not 2 be'
print(phrase.replace('2', 'to', 1))

ip = ':car::jeep::'
print(ip.split(':'))

result = re.findall(r'[^:]+', ip)
print(result)

def anagram(str1, str2):
    lstr1 = str1.lower()
    lstr2 = str2.lower()
    print(lstr1, lstr2)

    return set(lstr1) == set(lstr2)


print(anagram('ABC', 'CBA'))
print(anagram('dog', 'god'))
print(anagram('beat', 'table'))
print(anagram('beat', 'abeT'))

def words(str):
    print(str.split())
    print(re.split(r'\W+', str))


words('there! fdf9 dfdf -gr_uni-dfd hj')
