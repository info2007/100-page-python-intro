####################################
## 7. Importing and creating modules
####################################
# import random
# import random as rd # alias all the lib
# from random import * # advice against that
from random import randrange, choice
# from math import factorial as fact # alias just a function


def get_random_int():
    for _ in range(5): # "_" indicate a throwaway variable name
        rand_int = randrange(11)
        print(rand_int)


def sqr(n):
    """
    Return the square of a number.
    :param n: input number
    :return: square of the number
    """
    return n * n


def fact(n):
    """docstring for fact"""
    x = 1
    for i in range(2, n + 1):
        x *= i
    return x

def sqr_root(n):
    """
    Return the squqre root of a number
    :param n: input number
    :return: square root of the number
    """
    return n ** 0.5

def main():
    num = 5
    print(f'{num}: square={sqr(num)} ; factorial={fact(num)}')
    print(f'random choice = {choice([1, 2, 3, 5, 7, 11, 13, 17, 19])}')



if __name__ == '__main__':
    main() # common practice with the main function

