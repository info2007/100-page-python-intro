### DICT

currencies = {'Australia': 'AUD', 'France': 'EUR', 'Germany': 'EUR'}
print(currencies['Germany'])
currencies['Austria'] = 'EUR'
print(currencies)

combativeness = {'Athens': 58, 'Sparta': 76, 'Corinth': 52, 'Thebes': 45, 'Delos': 45}
naval_value = {'Athens': 82, 'Sparta': 46, 'Corinth': 75, 'Thebes': 52}
print(combativeness['Sparta'])

combativeness['Athens'] += 5;
del combativeness['Delos']

land_value = dict(Athens=75, Sparta=87, Corinth=74, Thebes=69)
print(land_value)
economy = dict([('Athens', 78), ('Sparta', 65), ('Corinth', 86), ('Thebes', 84)])
print(economy)

cities = ('Athens', 'Sparta', 'Corinth', 'Thebes')
cohesion = dict.fromkeys(cities, 0)
print(cohesion)

# print(currencies['New Zeland'])  # KeyError

print(economy.get('Syracus', 'UNKNOWN'))

# for combat in combativeness:
#     print(f'{combat} = {combativeness[combat]}')

# print(list(combativeness))
# print(tuple(combativeness))

# a, *b, c = combativeness
# print(f'{a = } ; {b = } ; {c = }')

# Ex: de-deduplicate:
nums = [1, 4, 6, 22, 3, 5, 4, 3, 6, 2, 1, 51, 3, 1]
dedup = {}
for num in nums:
    dedup[num] = dedup.get(num, 0) + 1
print(tuple(dedup))

print('Sparta' in economy)
print('Lyon' in economy)
print(economy.keys())
print(economy.values())
print(economy.items())

# print(economy.pop('Sparta'))
# print(economy.popitem())

print(economy)
economy.update(dict(Sydney=12, Athens=81))
economy.update([('Corinth', 82), ('Sparta', 67)])
economy.update({'Melbourne': 28})
print(economy)

fruits1 = {'banana': 12, 'papaya': 5, 'mango': 20}
fruits2 = {'mango': 10, 'fig': 100}
fruits = fruits1 | fruits2
# 26/08/2021: i have installed python 3.9 with homebrew,
# seems to be the new default python3 in a new shell BUT not in here?!... (stuck with 3.8)
# Oh; seems that i solved it: updating pycharm python interpreter of choice for this project
# but now i may miss some required packages by this project (see pycharm error messages:
# )
# 07/09/2021: I have installed the required packages, just following pycharm recommendations
# easy.
# it appears that those are the same requirements listed in the requirements.txt file here.
#fruits = {**fruits1, **fruits2}
print(fruits)

def many(**kwargs):
    print(f'{kwargs = }')


many()
# many(5) # TypeError:
many(we=5)
many(we=5, them=8)


def greeting(phrase='hello', style='='):
    print(f'{phrase:{style}^{len(phrase) + 6}}')


greeting()
data = {'phrase': 'Bonjour', 'style': '-'}
greeting(**data)
