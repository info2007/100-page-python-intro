empty_set = set()
print(empty_set)

nums = {2, -8, 2.3, 0, 2}
nums.add(10)
nums.add(20)
nums.update({20, 40, 60})
print(nums)

# wrong_set = {1, 2, [10, 20]} not possible to have mutable object as element,
# element need to be hashable.

print(2 in nums)
print(99 in nums)
# Since set uses hashtable (similar to dict keys), the lookup time is constant
# and much faster than ordered collections like list or tuple for large data sets.

color_1 = {'teal', 'light blue', 'green', 'yellow'}
color_2 = {'light blue', 'black', 'dark green', 'yellow'}
print(color_1.union(color_2))
print(color_1 | color_2)

print(color_1.intersection(color_2))
print(color_1 & color_2)

print(color_1.difference(color_2))
print(color_1 - color_2)
print(color_2.difference(color_1))
print(color_2 - color_1)

group1 = {'Peter', 'Jack', 'Joe'}
group2 = {'Peter', 'Jack'}
group3 = {'Paul', 'Pierre'}
print(group2.issubset(group1))
print(group1.issuperset(group2))
print(group3.isdisjoint(group1))

def has_duplicate(group):
    local_set = set(group)
    # print(len(group))
    # print(len(local_set))
    return len(group) > len(local_set)


print(has_duplicate((1, 2, 3, 3)))
print(has_duplicate((1, 2, 3, 4)))
print(has_duplicate('qwerty'))
print(has_duplicate('hardware'))
print(has_duplicate([3, 2, 3.0]))  # 3=3.0 here

