from math import factorial


# print('+++ Program compute factorial +++')
# while(True):
#     try:
#         num = input('Enter a positive integer:')
#         print(f'{num}! = {factorial(int(num))}')
#         break
#     except ValueError as e:
#         print(f'Exception thrown: not a positive integer, try again (exception msg:{e})')

def my_function_sum(n1, n2):
    types_allowed = (int, float)
    if type(n1) not in types_allowed or type(n2) not in types_allowed:
        raise TypeError('argument is not of the expected type')
    return n1 + n2

# n1 = 1
# n2 = 6
# try:
#     total = my_function_sum(n1, n2)
# except TypeError as e:
#     print(f'Invalid input data, error message: {e}')
# else:
#     print(f'{n1} + {n2} = {total}')
# finally:
#     print('END of the PROGRAM')


# Exercise:
# Write a function num(ip) that accepts a single argument and returns
# the corresponding integer or floating-point number contained in the
# argument. Only int, float and str should be accepted as valid input
# data type. Provide custom error message if the input cannot be
# converted to a valid number
def num(n):
    if type(n) not in [int, float, str]:
        print(f'The input need to be int, float or str ; "{n}" given. exit.')
        return
    sanitized_n = n
    if type(n) == str:
        sanitized_n = n.strip()
    try:
        float_n = float(sanitized_n)
        if float_n.is_integer():
            print(int(float_n))
        else:
            print(float_n)
    except ValueError as e:
        print('The input cannot be converted to a valid number. exit.')

    # if int(sanitized_n) == float(sanitized_n):
    #     print(int(sanitized_n))
    # else:
    #     print(float(sanitized_n))


'  blabla  '.strip()
num(4)
num('78')
num(67.2)
num(' \t 52 \t')
num(0x31)
num('3.982e5')
# num('0x31')
num(['1', '2.3'])