vowels = ['a', 'e', 'i', 'o', 'u', 'y']

nums = [0.0, 10, 20, 30, 40, 50]
print(nums)
nums[2] = 21
print(nums)
nums[-3:] = [32, 42, 52]
print(nums)
# automatic shrink/expand as neeeded:
nums[-3:] = [32, 42, 52, 63]
print(nums)
nums[1:5] = [1234]
print(nums)

# List methods and operations:
books = []
books.append('les fleurs')
books.append('le chercheur')
print(books)
books.extend(['L envie', 'good hope'])
print(books)
books.extend(range(3))
print(books)

letters = []
letters.extend('dispositif pour repecher les plantes')
print(letters.count('e'))
print(letters.count('z'))
print(letters.index('e'))

print('count/index on complex list:')
items = ['blabla', 4, 'c', ['x1', 'y1'], 4, 8]
print(items.index('blabla'))
print(items.index(['x1', 'y1']))
# print(items.index(['x2', 'y2'])) # ValueError: ['x2', 'y2'] is not in list

print('pop, del, remove, clear:')
primes = [2, 3, 5, 7, 11, 13]
print(primes.pop())
print(primes.pop(1))
print(primes)
nums = [-0.9, -0.5, 0, 0.5, 1, 2, 3.5, 3.51, 3.52]
del nums[0]
print(nums)
del nums[6:]
print(nums)
nums.remove(3.5)
print(nums)
nums.clear()
print(nums)

print('insert, reverse:')
books = ['how to make sourdough', 'the martien', 'green table']
books.insert(0, 'how to ground flour')
print(books)
books.insert(2, 'how slice sourdough')
print(books)
# books.insert(4, ['how to again', 'how to encore'])
# print(books)
books.reverse()
print(books)

# HEREHEREHERE: Sorting and company
nums = [4, 2, 16, 32, 8]
nums.sort(reverse=True)
print(nums)
output = sorted('capitalization')
buffer = ''
for c in output:
    buffer += c
print(buffer)
nums = [4, -2, 16, -32, 8, 128, -64]
nums.sort()
print(nums)
nums.sort(key=abs)
print(nums)

import random

nums = list(range(1, 11))
print(random.choice(nums))
random.shuffle(nums)
print(nums)

# Map, Filter and Reduce
nums = [2, 4, 8, 16, 32]
print(sum(nums))

print('xxx product xxx')


def product(input):
    total = 1.0
    for i in input:
        total *= i
    return total


print(product([-4, 2.3e12, 77.23, 982, 0b101]))
print(product(range(2, 6)))
# product(())
# product(['a', 'b'])


def remove_dunder(item):
    # all_functions = dir(item)
    # print(all_functions)
    functions = []
    for fct in dir(item):
        if not fct.startswith('__') or not fct.endswith('__'):
            functions.append(fct)
    return functions


print('str normal functions:', remove_dunder(str))
print('tuple normal functions:', remove_dunder(tuple))
print('list normal functions:', remove_dunder(list))
