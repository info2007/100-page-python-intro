# Tuple and Sequence operations


# Sequences and iterables
# iterable is An object capable of returning its members one at a time.
# Examples of iterables include all sequence types (such as list, str,
# and tuple) and some non-sequence types like dict, file objects, and
# objects of any classes you define with an __iter__() method or with a
# __getitem__() method that implements Sequence semantics.

# a sequence is An iterable which supports efficient element access
# using integer indices via the __getitem__() special method and
# defines a __len__() method that returns the length of the sequence.
# Some built-in sequence types are list, str, tuple, and bytes. Note
# that dict also supports __getitem__() and __len__(), but is
# considered a mapping rather than a sequence because the lookups use
# arbitrary immutable keys rather than integers.

# tuple () is immutable whereas a list [] is mutable

# little bit of DEFINITION:
#  tuple: a finite order list (FR=  "n-uplet", comme triplet, quadruplet etc)

def min_max_dum(arg):
    return min(arg), max(arg)


def min_max_optim(arg):
    min_optim = max_optim = arg[0]
    for i in arg:
        if i < min_optim:
            min_optim = i
        if i > max_optim:
            max_optim = i
    return min_optim, max_optim


print('dum:', min_max_dum('visualisation'))
print('dum:', min_max_dum((5, 1, 10)))

print('optim:', min_max_optim('visualisation'))
print('optim:', min_max_optim((5, 1, 10)))

primes = (2, 3, 5, 7, 11)
for paire in enumerate(primes, start=1):
    print(paire)


def many_args(a, *args):
    print(f'{a = } {args = }')


many_args('aaa', 'bb', 'cc')


def sum_nums(*args, initial=0):
    total = initial
    for arg in args:
        total += arg
    return total


print(sum_nums(1, 2, 3, 4, 5, initial=1000))
# print(sum_nums(initial=5, 2))
nums = 2, 3, 4
print(sum_nums(*nums, initial=1))


def sum_of_products(nums_a, nums_b):
    total = 0
    for a, b in zip(nums_a, nums_b):
        total += a*b
    return total


print(sum_of_products((1, 3, 5), (2, 4, 6)))
