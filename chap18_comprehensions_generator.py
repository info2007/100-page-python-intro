# COMPREHENSIONS
import math

nums = (321, 1, 1, 0, 5.3, 2)
# manual implementation
sqr_nums = []
for num in nums:
    sqr_nums.append(num * num)
print(sqr_nums)

# comprehension
res = [num * num for num in nums]
print(res)

# Filtering: 3 ways:
all_fcts = dir(dict)
print(all_fcts)
# manual
exposed_fcts = []
for fct in all_fcts:
    if not fct.startswith('__'):
        exposed_fcts.append(fct)
print(exposed_fcts)
# using filter
exposed_fcts = list(filter(lambda fct: not fct.startswith('__'), all_fcts))
print(exposed_fcts)
# comprehension syntax
exposed_fcts = [fct for fct in all_fcts if not fct.startswith('__')]
print(exposed_fcts)

squares = [i * i for i in range(1, 11) if i % 2 == 0]
print(squares)

TAX_RATE = 0.2
prices = [15, 45, 89, 100]
print([price * (TAX_RATE + 1) for price in prices])

nums = [9, -43, 1, 16, -99]
rsquares = [math.sqrt(num) if num > 0 else 0 for num in nums]
print(rsquares)

# BTW: about zip
zip_res = zip(range(1, 4), ['a', 'b', 'c', 'd'], ['i', 'ii', 'iii', 'iv']);
# for d, l, r in zip_res:
#     print(f'{d} - {l} - roman:{r}')
lres = list(zip_res)
print(lres)
un_res = zip(*lres)  # the inverse of zip is...zip!
plun_res = list(un_res)
print(plun_res)

letters = ['b', 'a', 'd', 'c']
numbers = [2, 4, 3, 1]
data1 = list(zip(letters, numbers))
data1.sort()
print(data1)
print(sorted(zip(numbers, letters)))

# BTW: enumerate
seasons = ['Spring', 'Summer', 'Fall', 'Winter']
print(list(enumerate(seasons, 5)))
print(seasons[1:])

## a bit of background on yield <- Generators <- Iterators


### Iterables
# When creating a list you can read items one by one, it's called iteration:
mylist = [1, 2, 3]
for i in mylist:
    print(i)
# mylist is an "iterable", si is an comprehension. We could write that iteration too:
mylist = [i ** i for i in range(4)]
print(mylist)
for i in mylist:
    print(i)

# iterable are handy in programmation: cause this is "list of things" processing.
# But when scaling up (which always happen at some time in prog) you can easily
# endup writing iterative code on very big number of item list... and that bring back
# performance concern (mainly mylist have to live in memory somewhere because this
# code requires it and generating list can be big CPU effort). Here come 1 element of the solution:

### Generator:
# Generators are iterator, a kind of iterables you can only iterate once, ie they
# do NOT store the value in memory, do NOT generate all the value at once,
# they generate the value on the fly:
mygenerator = (i ** i for i in range(4))
print(mygenerator)  # <generator object <genexpr> at 0x10feb4580>
for i in mygenerator:
    print(i)


# Although the syntax is close, the underlying machine implementation and memory
# structure is very different (And the possibilities are not so wide.)
# the code above is using the "generator expression", but a more generic way
# to write this would be:
def power_to_self():
    print('fct:power_to_self:begin')
    for i in range(4):
        print('fct:power_to_self:loop:begin')
        yield i ** i
    print('fct:power_to_self:loop:end')
    yield 4 ** 4  # sneaking in an extra element Just for the exercise
    print('fct:power_to_self:end.')


print('init generator power_to_self')
mygenerator2 = power_to_self()  # nothing is run yet
print('print generator power_to_self')
print(mygenerator2)
print('iterate generator power_to_self')
for i in mygenerator2:  # now we are starting.
    print(i)
print('end')


# IMO a generator implementation is 2 things: a code routine + some variable in memory, to store the current progress
# DONE: what is next usage and what the benefit of itertool (see first answer in
# https://stackoverflow.com/questions/231767/what-does-the-yield-keyword-do )
# HEREHERE: follon on the tutoriel, yield section...
# link: https://learnbyexample.github.io/100_page_python_intro/comprehensions-and-generator-expressions.html

# exercises

# Write a function that returns a dictionary sorted by values in ascending order.
def sort_by_value(d):
    # option A:
    # return dict(sorted(d.items(), key=lambda item: item[1]))
    # option B:
    return {k: v for k, v in sorted(d.items(), key=lambda item: item[1])}


marks = dict(Rahul=86, Ravi=92, Rohit=75, Rajan=79, Ram=92)
print(marks)
sorted_marks = sort_by_value(marks)
print(sorted_marks)
